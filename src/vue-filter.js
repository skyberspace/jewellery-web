import Vue from "vue";
import numeral from "numeral";

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

function formatDate(date) {
    var monthNames = [
        "Ocak",
        "Şubat",
        "Mart",
        "Nisan",
        "Mayıs",
        "Haziran",
        "Temmuz",
        "Ağustos",
        "Eylül",
        "Ekim",
        "Kasım",
        "Aralık"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return day + " " + monthNames[monthIndex] + " " + year;
}

function formatDateUs(date) {
    var d = new Date(date),
        month = "" + (d.getMonth() + 1),
        day = "" + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
}
// eslint-disable-next-line
function dateAsInput(a) {
    if (a != null) {
        try {
            let date = new Date(a);
            return formatDateUs(date);
        } catch (e) {
            var func = "return new " + a.replaceAll("\\", "").replaceAll("/", "");
            let date = new Function(func)();
            return formatDateUs(date);
        }
    }
    return "";
}
// eslint-disable-next-line
function timeAsInput(a) {
    if (a != null) {
        return a.Hours + ":" + a.Minutes;
    }
    return "";
}

Vue.filter("cevir", function (s) {
    return numeral(s).format("0,0[.]00");
});

Vue.filter("cevir2", function (s) {
    return numeral(s).format("0,0.00");
});

Vue.filter("buyut", function (s) {
    if (s != null) {
        return s.toString().toUpperCase();
    }
});

Vue.filter("date", function (a) {
    if (a != null) {
        try {
            let date = new Date(a);
            return formatDate(date);
        } catch (e) {
            var func = "return new " + a.replaceAll("\\", "").replaceAll("/", "");
            let date = new Function(func)();
            return formatDate(date);
        }
    }
    return "";
});

Vue.filter("dateTime", function (a) {
    if (a != null) {
        try {
            let date = new Date(a);
            return (
                formatDate(date) + " - " + date.getHours() + ":" + date.getMinutes()
            );
        } catch (e) {
            var func = "return new " + a.replaceAll("\\", "").replaceAll("/", "");
            let date = new Function(func)();
            return formatDate(date) + " " + date.getHours() + ":" + date.getMinutes();
        }
    }
    return "";
});

Vue.filter("time", function (a) {
    if (a != null) {
        return a.Hours + ":" + a.Minutes;
    }
    return "";
});

Vue.filter("bool", function (a) {
    return a ? "Evet" : "Hayır";
});
Vue.filter("kisalt", function (a) {


    var split = a.split(" ");
    var v1 = split[0];
    var v2 = split.length >= 2 ? split[1] : "";

    return v1 + " " + v2;

});



Vue.filter("siparisDurum", function (a) {
    var res = "";
    switch (a) {
        case 0: res = "Beklemede"; break;
        case 1: res = "Onaylı"; break;
        case 2: res = "Hazırlanıyor"; break;
        case 3: res = "Hazır"; break;
    }
    return res;
});
export default {};
