import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Jewelry_Area from "../components/Jewelry_Area.vue";
import About from "../components/About.vue";
import Contact from "../components/Contact.vue";
import Account from "../components/Account.vue";
import CartPage from "../components/CartPage.vue";
import ProductDetail from "../components/Product-Detail.vue";
import Auth from "../components/Auth.vue";


Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  ,
  {
    path: "/set/:id",
    component: Jewelry_Area,
    props: true,
  },
  {
    path: "/urun/:id",
    component: ProductDetail,
    props: true,
  },
  {
    path: "/hakkimizda",
    component: About
  },
  {
    path: "/iletisim",
    component: Contact
  },
  {
    path: "/hesap-olustur",
    component: Auth
  },
  {
    path: "/hesabim",
    component: Account
  },
  {
    path: "/sepet",
    component: CartPage
  }
  /*{
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about"  '../views/About.vue')
  }*/
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
