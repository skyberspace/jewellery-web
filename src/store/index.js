import Vue from "vue";
import Vuex from "vuex";
import api from "../api";
import router from "../router/";
import VuexPersistence from 'vuex-persist'
const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})



Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    test: "",
    loginPage: {
      lText: "",
      lError: false
    },
    user: null,
  },
  getters: {
    isLoggedIn(state) {
      return state.user != null;
    }
  },
  mutations: {
    async Login(state, payload) {
      var res = await api.Request("Auth/Login", { email: payload.email, password: payload.password });
      if (res.status) {
        store.commit("LoginInner", res);
      } else {
        state.loginPage.lText = res.message;
        state.loginPage.lError = true;
      }
      store.commit("Test");
    },
    async LoginInner(state, payload) {
      Vue.set(state, "user", payload.user);
      Vue.cookie.set("userToken", payload.token, { expires: "432000s" });
      if (window.loggedInAppHook)
        await window.loggedInAppHook();
      state.loginPage.lText = "";
      state.loginPage.lError = false;
      router.replace("/");
    },
    async Logout(state) {
      state.loginPage.lError = false;
      state.loginPage.lText = "";
      state.user = null;
      Vue.cookie.set("userToken", "", { expires: "10s" });

      store.commit("Test");
      router.push("/hesap-olustur");
    }
  },
  actions: {},
  plugins: [vuexLocal.plugin]
});

export default store;
