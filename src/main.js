import Vue from "vue";
import UUID from "vue-uuid";
import App from "./App.vue";
import vueCookie from "vue-cookie";
import router from "./router";
import store from "./store";
import "./assets/css/plugins.css";
import "./assets/scss/style.scss";
import "./assets/css/font-awesome/css/all.min.css";
import api from "./api"
import Fragment from 'vue-fragment'

import "./assets/custom/custom.scss"


require("./vue-filter");

Vue.use(Fragment.Plugin)
Vue.use(UUID);
Vue.use(vueCookie);

Vue.config.productionTip = false;


Vue.prototype.$api = api;


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
